﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Film
/// </summary>
public static class FilmConsoleExtensions
{
    /// <summary>
    /// Méthode qui affiche l'information d'un film à la console
    /// </summary>
    /// <param name="film">Film</param>
    public static void AfficherConsole(this Film? film)
    {
        if (film != null)
        {
            Console.WriteLine($"Id : {film.FilmId}");
            Console.WriteLine($"Nom : {film.Titre}");
            Console.WriteLine($"Durée : {film.Duree} minute(s)");
            Console.WriteLine($"Étoile : {film.Etoile}");
            Console.WriteLine($"Budget : {film.Budget:N2}");
            Console.WriteLine($"Date Sortie : {film.DateSortie:d MMMM yyyy}");
        }
        else
        {
            Console.WriteLine("Film non trouvé.");
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste de films à la console
    /// </summary>
    /// <param name="lstFilm"></param>
    public static void AfficherConsole(this List<Film> lstFilm)
    {
        if (lstFilm?.Count > 0)
        {
            foreach (Film film in lstFilm)
            {
                film.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucun film dans la base de données");
        }
    }
}
