﻿namespace GestionPersonnageApp.Extensions.Data;

/// <summary>
/// Classe statique qui regroupe les méthodes d'extension pour la console du modèle Univers
/// </summary>
public static class UniversConsoleExtensions
{
    /// <summary>
    /// Méthode qui affiche l'information d'un univers à la console
    /// </summary>
    /// <param name="univers">Univers</param>
    public static void AfficherConsole(this Univers? univers)
    {
        if (univers != null)
        {
            Console.WriteLine($"Id : {univers.UniversId}");
            Console.WriteLine($"Nom : {univers.Nom}");
            Console.WriteLine($"Année de création : {univers.AnneeCreation}");
            Console.WriteLine($"Site Web : {univers.SiteWeb}");
            Console.WriteLine($"Propriétaire : {univers.Proprietaire}");
        }
        else
        {
            Console.WriteLine("Univers non trouvé.");
        }
    }

    /// <summary>
    /// Méthode qui affiche l'information d'une liste d'univers à la console
    /// </summary>
    /// <param name="lstUnivers"></param>
    public static void AfficherConsole(this List<Univers> lstUnivers)
    {
        if (lstUnivers?.Count > 0)
        {
            foreach (Univers univers in lstUnivers)
            {
                univers.AfficherConsole();
                Console.WriteLine(Environment.NewLine);
            }
        }
        else
        {
            Console.WriteLine("Il n'y a aucun univers dans la base de données");
        }
    }
}
