﻿global using GestionPersonnageApp; //Les classes à la racine de l'application
global using GestionPersonnageApp.Data; //Les classes du modèle du contexte
global using GestionPersonnageApp.Data.Context; //La classe du contexte
global using GestionPersonnageApp.Managers; //Les classes de type Manager
global using GestionPersonnageApp.Repositories; //Les classes de type Repository
global using GestionPersonnageApp.Services; //Les classes de type Service
global using GestionPersonnageApp.Extensions.Data; //Extensions des modèles du contexte
global using Microsoft.Extensions.DependencyInjection; //Les classes pour l'injection de dépendances
global using Microsoft.EntityFrameworkCore; //Les classes et les méthodes de Entity Framework
