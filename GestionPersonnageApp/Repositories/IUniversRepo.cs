﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Univers
/// </summary>
public interface IUniversRepo
{
    /// <summary>
    /// Obtenir un univers à partir de sa clé primaire
    /// </summary>
    /// <param name="universId">Clé primaire</param>
    /// <returns>L'univers s'il existe, sinon null si non trouvé</returns>
    Univers? ObtenirUnivers(int universId);

    /// <summary>
    /// Obtenir la liste de tous les univers
    /// </summary>
    /// <returns>Liste des univers de la base de données</returns>
    List<Univers> ObtenirListe();
}