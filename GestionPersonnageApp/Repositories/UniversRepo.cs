﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Classe qui contient les méthodes de communication avec la base de données pour la table Univers
/// </summary>
public class UniversRepo : IUniversRepo
{
    private readonly GestionPersonnageContext _db;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="db">Contexte de la base de données GestionPersonnage</param>
    public UniversRepo(GestionPersonnageContext db)
    {
        _db = db;
    }

    public List<Univers> ObtenirListe()
    {
        return _db.Univers.ToList();
    }

    public Univers? ObtenirUnivers(int universId)
    {
        return _db.Univers.Where(u => u.UniversId == universId).FirstOrDefault();
    }
}
