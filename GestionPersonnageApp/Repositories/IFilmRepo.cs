﻿namespace GestionPersonnageApp.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Film
/// </summary>
public interface IFilmRepo
{
    /// <summary>
    /// Obtenir un film à partir de sa clé primaire
    /// </summary>
    /// <param name="FilmId">Clé primaire</param>
    /// <returns>Le film s'il existe, sinon null si non trouvé</returns>
    Film? ObtenirFilm(int FilmId);

    /// <summary>
    /// Obtenir la liste de tous les films
    /// </summary>
    /// <returns>Liste des films de la base de données</returns>
    List<Film> ObtenirListe();
}