﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Interface qui contient les services du modèle Univers
/// </summary>
public interface IUniversService
{
    /// <summary>
    /// Obtenir la liste des univers
    /// </summary>
    /// <returns>La liste des univers</returns>
    List<Univers> ObtenirListe();

    /// <summary>
    /// Obtenir un univers en fonction de sa clé primaire
    /// </summary>
    /// <param name="universId">Clé primaire de l'univers</param>
    /// <returns>L'univers correspondant à la clé primaire ou null si non trouvé</returns>
    Univers? ObtenirUnivers(int universId);
}