﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Interface qui contient les services du modèle Film
/// </summary>
public interface IFilmService
{
    /// <summary>
    /// Obtenir la liste des films
    /// </summary>
    /// <returns>La liste des films</returns>
    List<Film> ObtenirListe();

    /// <summary>
    /// Obtenir un film en fonction de sa clé primaire
    /// </summary>
    /// <param name="filmId">Clé primaire du film</param>
    /// <returns>Le film correspondant à la clé primaire ou null si non trouvé</returns>
    Film? ObtenirFilm(int filmId);
}