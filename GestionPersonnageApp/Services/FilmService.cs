﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Film
/// </summary>
public class FilmService : IFilmService
{
    private readonly IFilmRepo _filmRepo;

    public FilmService(IFilmRepo filmRepo)
    {
        _filmRepo = filmRepo;
    }

    public Film? ObtenirFilm(int filmId)
    {
        return _filmRepo.ObtenirFilm(filmId);
    }

    public List<Film> ObtenirListe()
    {
        return _filmRepo.ObtenirListe();
    }
}
