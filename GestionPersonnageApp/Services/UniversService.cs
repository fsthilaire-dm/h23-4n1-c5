﻿namespace GestionPersonnageApp.Services;

/// <summary>
/// Classe qui contient les services du modèle Univers
/// </summary>
public class UniversService : IUniversService
{
    private readonly IUniversRepo _universRepo;

    public UniversService(IUniversRepo universRepo)
    {
        _universRepo = universRepo;
    }

    public Univers? ObtenirUnivers(int universId)
    {
        return _universRepo.ObtenirUnivers(universId);
    }

    public List<Univers> ObtenirListe()
    {
        return _universRepo.ObtenirListe();
    }
}
