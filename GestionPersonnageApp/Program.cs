﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using GestionPersonnageApp.Extensions.ServiceCollections;

//La variable args sont les arguments du programme. Ils seront accessibles en tout temps à partir de Environment.GetCommandLineArgs()
//À noter que l'index 0 est toujours le nom du DLL exécuté.

//Creation du configurateur de l'application

var builder = Host.CreateDefaultBuilder(args);

builder.ConfigureLogging(logging =>
{
    logging.ClearProviders();    
});

//Configuration des services
builder.ConfigureServices((context, services) =>
{
    services.AddTransient<App>(); //Application principale

    //Enregistrement du contexte    
    services.AddDbContext<GestionPersonnageContext>(options => options.UseSqlServer());

    services.EnregistrerManagers();
    services.EnregistrerServices();
    services.EnregistrerRepositories();
});

//Création du host de l'application en fonction de sa configuration
var host = builder.Build();

//Démarrage de l'application
App application = host.Services.GetRequiredService<App>();
await application.DemarrerApplicationAsync();