﻿namespace GestionPersonnageApp.Managers;

/// <summary>
/// Interface qui s'occupe de la coordination du modèle Univers
/// </summary>
public interface IUniversManager
{
    /// <summary>
    /// Afficher tous les univers
    /// </summary>
    void AfficherListe();

    /// <summary>
    /// Afficher un univers en fonction de sa clé primaire
    /// </summary>        
    void AfficherParId();
}